//Import model prize 
const prizeModel = require('../models/prizeModel');

// Khai báo thư viện mongoose để tạo _id
const mongoose = require("mongoose");

const createPrize = (request, response) => {
  // B1: Thu thập dữ liệu
  let bodyRequest = request.body;
  // B2: Kiểm tra dữ liệu
  if (!bodyRequest.name) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "name is required"
    })
  }
  // B3: Thao tác với cơ sở dữ liệu
  let createPrize = {
    _id: mongoose.Types.ObjectId(),
    name: bodyRequest.name,
    description: bodyRequest.description
  }

  prizeModel.create(createPrize, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(201).json({
        status: "Success: Prize created",
        data: data
      })
    }
  })
}

const getAllPrize = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  //B2: Validate dữ liệu
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.find((error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get all prizes success",
        data: data
      })
    }
  })
}

const getPrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "prizeId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.findById(prizeId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Get prize by ID success",
        data: data
      })
    }
  })
}

const updatePrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  let bodyRequest = request.body;

  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "prizeId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  let prizeUpdate = {
    name: bodyRequest.name,
    description: bodyRequest.description
  }

  prizeModel.findByIdAndUpdate(prizeId, prizeUpdate, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(200).json({
        status: "Success: Update prize by ID success",
        data: data
      })
    }
  })
}

const deletePrizeById = (request, response) => {
  //B1: Chuẩn bị dữ liệu
  let prizeId = request.params.prizeId;
  //B2: Validate dữ liệu
  if (!mongoose.Types.ObjectId.isValid(prizeId)) {
    return response.status(400).json({
      status: "Error 400: Bad Request",
      message: "prizeId is not valid"
    })
  }
  //B3: Thao tác với cơ sở dữ liệu
  prizeModel.findByIdAndDelete(prizeId, (error, data) => {
    if (error) {
      return response.status(500).json({
        status: "Error 500: Internal server error",
        message: error.message
      })
    } else {
      return response.status(204).json({
        status: "Success: Delete prize success"
      })
    }
  })
}

module.exports = {
  createPrize,
  getAllPrize,
  getPrizeById,
  updatePrizeById,
  deletePrizeById
}