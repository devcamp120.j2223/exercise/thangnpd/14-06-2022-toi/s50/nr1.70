// Import bộ thư viện express
const express = require('express'); 

//Import middleware
const luckydiceMiddleware = require('../middlewares/luckydiceMiddleware')

//Tạo 1 instance router
const router = express.Router();

//Import controller
const {
  createPrize,
  getAllPrize,
  getPrizeById,
  updatePrizeById,
  deletePrizeById
} = require('../controllers/prizeController');

router.use('/', luckydiceMiddleware);

router.post('/prizes', createPrize);

router.get('/prizes', getAllPrize);

router.get('/prizes/:prizeId', getPrizeById);

router.put('/prizes/:prizeId', updatePrizeById);

router.delete('/prizes/:prizeId', deletePrizeById);

module.exports = router;